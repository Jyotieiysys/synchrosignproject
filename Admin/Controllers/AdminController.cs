﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SynchrosignNew.Admin.IServices;
using SynchrosignNew.Admin.Models.Model;
using SynchrosignNew.Areas.Models.Model;
using SynchrosignNew.Areas.User.Models.Model;
using SynchrosignNew.Filters;
using SynchrosignNew.Identity;

namespace SynchrosignNew.Admin.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IAdminService _adminService;
        private readonly UserManager<UserEntity> _userManager;
        private object _userService;
        private IHostingEnvironment _environment;

        public AdminController(IAdminService adminService, UserManager<UserEntity> userManager, IHostingEnvironment environment)
        {
            _adminService = adminService;
            _userManager = userManager;
            _environment = environment;
        }

        [Authorized]
        [HttpGet("AccountBlock")]
        public IActionResult AccountBlock(string AccountNo)

        {
            if (!ModelState.IsValid) return BadRequest(new ApiError(ModelState));
            Claim claim = HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();
            UserEntity usr = _userManager.FindByIdAsync(claim.Value).GetAwaiter().GetResult();
            string rolename = _userManager.GetRolesAsync(usr).GetAwaiter().GetResult().FirstOrDefault();
            if (rolename.ToLower() == "admin")
            {
                var (result, succeeded) = _adminService.AccountBlock(AccountNo);
                if (succeeded) return Ok(result);
                return BadRequest(result);
            }
            return Unauthorized(new JsonResult("UnAuthorized User"));
        }

        [Authorized]
        [HttpGet("GetAllUserDetails")]
        public IActionResult GetAllUserDetails()
        {
            if (!ModelState.IsValid) return BadRequest(new ApiError(ModelState));
            Claim claim = HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();
            UserEntity usr = _userManager.FindByIdAsync(claim.Value).GetAwaiter().GetResult();          
            string rolename = _userManager.GetRolesAsync(usr).GetAwaiter().GetResult().FirstOrDefault();
            if (rolename.ToLower() == "admin")
            {
            var (result, succeeded) = _adminService.GetAllUserDetails();
            if (succeeded) return Ok(result);
            return BadRequest(result);
             }
            return Unauthorized(new JsonResult("UnAuthorized User"));
        }
        
        [Authorize]
        [HttpGet("GetUserByID")]
        public IActionResult GetUserByID(string ID)
        {
            if (!ModelState.IsValid) return BadRequest(new ApiError(ModelState));
            Claim claim = HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();
            UserEntity usr = _userManager.FindByIdAsync(claim.Value).GetAwaiter().GetResult();
            string rolename = _userManager.GetRolesAsync(usr).GetAwaiter().GetResult().FirstOrDefault();
            if (rolename.ToLower() == "admin")
            {
                var (result, succeeded) = _adminService.GetUserByID(ID);
                if (succeeded) return Ok(result);
                return BadRequest(result);
            }
            return Unauthorized(new JsonResult("UnAuthorized User"));

        }

        [Authorized]
        [HttpPost("UpdateEnableDisableUser")]
        public IActionResult UpdateEnableDisableUser([FromBody]EnableDisableModel enableDisableModel)
        {
            if (!ModelState.IsValid) return BadRequest(new ApiError(ModelState));
            Claim claim = HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();
            UserEntity usr = _userManager.FindByIdAsync(claim.Value).GetAwaiter().GetResult();
            string rolename = _userManager.GetRolesAsync(usr).GetAwaiter().GetResult().FirstOrDefault();
            if (rolename.ToLower() == "admin")
            {
                var (result, succeeded) = _adminService.UpdateEnableDisableUser(enableDisableModel);

                if (succeeded) return Ok(result);
                return BadRequest(result);
            }
            return Unauthorized(new JsonResult("UnAuthorized User"));
        }


        [Authorized]
        [HttpPut("EditUserProfile")]
        public IActionResult EditUserProfile([FromBody]UpdateUserDetails updateUserDetails)
        {

            if (!ModelState.IsValid) return BadRequest(new ApiError(ModelState));
            Claim claim = HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();
            UserEntity usr = _userManager.FindByIdAsync(claim.Value).GetAwaiter().GetResult();
            string rolename = _userManager.GetRolesAsync(usr).GetAwaiter().GetResult().FirstOrDefault();
            if (rolename.ToLower() == "admin")
            {
                var (result, succeeded) = _adminService.EditUserProfile(updateUserDetails);
                if (succeeded) return Ok(result);
                return BadRequest(result);
            }
            return Unauthorized(new JsonResult("UnAuthorized User"));


        }


        [Authorized]
        [HttpGet("GetAllUserByID")]
        public IActionResult GetAllUserByID(bool Status)
        {
            if (!ModelState.IsValid) return BadRequest(new ApiError(ModelState));
            Claim claim = HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();
            UserEntity usr = _userManager.FindByIdAsync(claim.Value).GetAwaiter().GetResult();
            string rolename = _userManager.GetRolesAsync(usr).GetAwaiter().GetResult().FirstOrDefault();
            if (rolename.ToLower() == "admin")
            {
                var (result, succeeded) = _adminService.GetAllUserByID(Status);
                if (succeeded) return Ok(result);
                return BadRequest(result);
            }
            return Unauthorized(new JsonResult("UnAuthorized User"));
        }

        [Authorized]
        [HttpPost("UploadImage")]
        public IActionResult UploadImage([FromForm]UploadImage file)
        {

            if (!ModelState.IsValid) return BadRequest(new ApiError(ModelState));
            Claim claim = HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();
            UserEntity usr = _userManager.FindByIdAsync(claim.Value).GetAwaiter().GetResult();
            string rolename = _userManager.GetRolesAsync(usr).GetAwaiter().GetResult().FirstOrDefault();
            if (rolename.ToLower() == "admin")
            {
                var (result, succeeded) = _adminService.UploadImages1(file.Image);
                if (succeeded) return Ok(result);
                return BadRequest(result);
            }
            return Unauthorized(new JsonResult("UnAuthorized User"));          
        }
    }
}