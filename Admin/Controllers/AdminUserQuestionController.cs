﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SynchrosignNew.Admin.IServices;
using SynchrosignNew.Areas.Models.Model;
using SynchrosignNew.Identity;

namespace SynchrosignNew.Admin.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminUserQuestionController : ControllerBase
    {
        private readonly IAdminService _adminService;
        public AdminUserQuestionController(IAdminService adminService)
        {
            _adminService = adminService;
        }

        [HttpPost("AddandUpdateAdminUserQuestion")]
        public IActionResult AddandUpdateAdminUserQuestion([FromBody]AdminUserSecurityQuestion securityQuestion)
        {
         
            if (!ModelState.IsValid) return BadRequest(new ApiError(ModelState));
            var (result, succeeded) = _adminService.AddandUpdateAdminUserQuestion(securityQuestion);
            if (succeeded) return Ok(result);
            return BadRequest(result);
        }
        [HttpGet("GetAllAdminUserQuestion")]
        public IActionResult GetAllAdminUserQuestion()
        {
            if (!ModelState.IsValid) return BadRequest(new ApiError(ModelState));
            var (result, succeeded) = _adminService.GetAllAdminUserQuestion();
            if (succeeded) return Ok(result);
            return BadRequest(result);
        }
        [HttpGet("GetAdminUserQuestionbyid")]
        public IActionResult GetAdminUserQuestionbyid(int QuestionId)
        {
            if (!ModelState.IsValid) return BadRequest(new ApiError(ModelState));
            var (result, succeeded) = _adminService.GetAdminUserQuestionbyid(QuestionId);
            if (succeeded) return Ok(result);
            return BadRequest(result);
        }
    }
}