﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SynchrosignNew.Admin.Models.Model;
using SynchrosignNew.Areas.User.Models.Model;
using SynchrosignNew.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SynchrosignNew.Admin.IServices
{
    public interface IAdminService
    {

        (JsonResult result, bool Succeeded) AccountBlock(string AccountNo);
        (JsonResult result, bool succeeded) GetAllUserDetails();
        (JsonResult result, bool succeeded) GetUserByID(string ID);
        (JsonResult result, bool Succeeded) UpdateEnableDisableUser(EnableDisableModel enableDisableModel);
        (JsonResult result, bool Succeeded) EditUserProfile(UpdateUserDetails form);

        (JsonResult result, bool succeeded) GetAllUserByID(bool Status);

        (JsonResult data, bool status) UploadImages1(IFormFile file);
        (JsonResult result, bool succeeded) AddandUpdateAdminUserQuestion(AdminUserSecurityQuestion securityQuestion);
        (JsonResult result, bool succeeded) GetAllAdminUserQuestion();
        (JsonResult result, bool succeeded) GetAdminUserQuestionbyid(int QuestionId);
        //(JsonResult result, bool Succeeded) SendEmailMy();


    }
}
