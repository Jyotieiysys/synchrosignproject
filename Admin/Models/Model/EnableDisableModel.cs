﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SynchrosignNew.Admin.Models.Model
{
    public class EnableDisableModel
    {
        public string ID { get; set; }
        public Boolean Status { get; set; }

    }
}
