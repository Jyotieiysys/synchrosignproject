﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SynchrosignNew.Admin.Models.Model
{
    public class UploadImage
    {

        public string Name { get; set; }
        public IFormFile Image { get; set; }

        public string ImageContent { get; set; }
    }
}
