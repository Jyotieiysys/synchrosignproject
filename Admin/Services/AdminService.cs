﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SynchrobitExchange.Areas.User.Models;
using SynchrosignNew.Admin.IServices;
using SynchrosignNew.Admin.Models.Model;
using SynchrosignNew.Areas.User.Context;
using SynchrosignNew.Areas.User.InterFace;
using SynchrosignNew.Areas.User.Models.Model;
using SynchrosignNew.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace SynchrosignNew.Admin.Services
{
    public class AdminService : IAdminService
    {
        private readonly SynchrosignContext _synchrosignContext;
        private readonly UserManager<UserEntity> _userManager;
        private readonly ICommonService _commonService;
        IConfiguration _configuration;
        private static IAmazonS3 client;
        private static string bucketName;

        public AdminService(SynchrosignContext synchrosignContext, UserManager<UserEntity> userManager, IConfiguration configuration, ICommonService commonService)
        {
            _synchrosignContext = synchrosignContext;
            _userManager = userManager;
            _configuration = configuration;
            _commonService = commonService;

            var accessKey = _configuration.GetSection("AWS:AccessKey").Value;
            var secretKey = _configuration.GetSection("AWS:SecretKey").Value;
            AmazonS3Config clientConfig = new AmazonS3Config();
            clientConfig.SignatureVersion = "4";
            clientConfig.RegionEndpoint = RegionEndpoint.USEast2;
            clientConfig.SignatureMethod = Amazon.Runtime.SigningAlgorithm.HmacSHA256;
            client = new AmazonS3Client(accessKey, secretKey, clientConfig);
            bucketName = _configuration.GetSection("AWS:BucketName").Value;
        }

        public (JsonResult result, bool Succeeded) AccountBlock(string AccountNo)
        {
            try
            {
                var user = _userManager.Users.Where(u => u.AccountNo == AccountNo).First();
                user.Status = "Deactive";
                var result = _userManager.UpdateAsync(user).GetAwaiter().GetResult();
                if (result.Succeeded)
                {
                    return (new JsonResult("User account blocked successfully."), true);
                }
                else
                {
                    return (new JsonResult(result.Errors), false);
                }
            }
            catch (Exception ex)
            {
                return (new JsonResult("Some server error has occurred. Please try again!!"), false);
            }
        }

        public (JsonResult result, bool succeeded) GetAllUserDetails()
        {
            try
            {
                var details = _synchrosignContext.Users.ToList();
                return (new JsonResult(details), true);

            }
            catch (Exception ex)
            {
                return (new JsonResult("Some server error occured. Please try again!!"), false);
            }
        }


        public (JsonResult result, bool succeeded) GetUserByID(string ID)
        {
            try
            {
                var Details = _synchrosignContext.Users.Where(x => x.Id.ToString() == ID).FirstOrDefault();
                return (new JsonResult(Details), true);

            }
            catch (Exception ex)
            {
                return (new JsonResult("Some server error occured. Please try again!!"), false);
            }

        }

        public (JsonResult result, bool Succeeded) UpdateEnableDisableUser(EnableDisableModel enableDisableModel)
        {
            try
            {
                var Details = _synchrosignContext.Users.Where(x => x.Id.ToString() == enableDisableModel.ID).FirstOrDefault();
                if (enableDisableModel.Status)
                {
                    Details.Status = "Active";
                }
                else
                {
                    Details.Status = "Deactive";
                }

                var result = _userManager.UpdateAsync(Details).GetAwaiter().GetResult();
                if (result.Succeeded)
                {
                    if (enableDisableModel.Status)
                    {
                        EmailInformation emailInformation = new EmailInformation();
                        emailInformation.EmailAddress = Details.Email;
                        var (Succeeded, Error) = _commonService.SendEmail("ENABLEACCOUNT", emailInformation);
                        return (new JsonResult("User Enable by Admin."), true);
                    }
                    else
                    {
                        EmailInformation emailInformation = new EmailInformation();
                        emailInformation.EmailAddress = Details.Email;
                        var (Succeeded, Error) = _commonService.SendEmail("DISABLEACCOUNT", emailInformation);
                        return (new JsonResult("User Disable by Admin."), true);
                    }

                }
                else
                {
                    return (new JsonResult("Ivalid Data."), true);
                }

            }
            catch (Exception ex)
            {
                return (new JsonResult("Some server error occured. Please try again!!"), false);
            }
        }
        public (JsonResult result, bool Succeeded) EditUserProfile(UpdateUserDetails form)
        {
            string message = "Profile details Updated.";
            try
            {
                var user = _userManager.FindByIdAsync(form.Id).GetAwaiter().GetResult();
                if (user != null)
                {
                    user.FullName = form.FullName;
                    user.ModifiedDate = DateTime.Now;
                    user.SelectNationality = form.Nationality;
                    user.UserName = form.UserName;
                    user.PhoneNumber = form.PhoneNumber;
                }
                var res = _userManager.UpdateAsync(user).GetAwaiter().GetResult();
                if (res.Succeeded)
                {
                    return (new JsonResult(message), true);
                }
                else
                {
                    return (new JsonResult(res.Errors.FirstOrDefault()?.Description), false);
                }
            }
            catch (Exception ex)
            {
                return (new JsonResult("Some server error occured. Please try again!!"), false);
            }

        }

        public (JsonResult result, bool succeeded) GetAllUserByID(bool Status)
        {
            try
            {
                var status = "";
                if (Status)
                {
                    status = "Active";
                }
                else
                {
                    status = "Deactive";
                }
                var details = _synchrosignContext.Users.Where(x => x.Status.ToLower() == status.ToLower()).ToList();

                return (new JsonResult(details), true);
            }
            catch (Exception ex)
            {
                return (new JsonResult("Some server error occured. Please try again!!"), false);

            }
        }

        public (JsonResult data, bool status) UploadImages1(IFormFile file)
        {

            try
            {// connecting to the client
             // var client = new AmazonS3Client(s3ConfigurationModel.AccessKey, s3ConfigurationModel.SecretKey, RegionEndpoint.USEast2);

                // get the file and convert it to the byte[]
                byte[] fileBytes = new Byte[file.Length];
                file.OpenReadStream().Read(fileBytes, 0, Int32.Parse(file.Length.ToString()));

                // create unique file name for prevent the mess
                var fileName = Guid.NewGuid() + file.FileName;

                PutObjectResponse response = null;

                using (var stream = new MemoryStream(fileBytes))
                {
                    var request = new PutObjectRequest
                    {
                        BucketName = bucketName,
                        Key = fileName,
                        InputStream = stream,
                        ContentType = file.ContentType,
                        CannedACL = S3CannedACL.PublicRead
                    };

                    response = client.PutObjectAsync(request).GetAwaiter().GetResult();
                };

                if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    // this model is up to you, in my case I have to use it following;
                    var url = Generatepresignedurl(bucketName, fileName, 6000);
                    // var Details = new object
                    URLModel uRLModel = new URLModel
                    {
                        FileName = fileName,
                        ImageURL = url
                    };


                    //return (new JsonResult(new Dictionary<object, object>
                    //  {
                    // { "key", fileName },
                    // { "imageUrl", url }
                    // }), true);
                    return (new JsonResult(uRLModel), true);
                }
                else
                {
                    // this model is up to you, in my case I have to use it following;
                    return (new JsonResult("Some server error occured. Please try again!!"), false);
                }
            }
            catch (Exception Ex)
            {
                return (new JsonResult("Some server error occured. Please try again!!"), false);
            }
        }

        public string Generatepresignedurl(string bucketName, string key, int expireinseconds)
        {
            try
            {
                string urlString = string.Empty;
                GetPreSignedUrlRequest request = new GetPreSignedUrlRequest
                {
                    BucketName = bucketName,
                    Key = key,
                    Expires = DateTime.Now.AddSeconds(expireinseconds)

                };
                urlString = client.GetPreSignedURL(request);
                return urlString;
            }
            catch (Exception ex)
            {
                return "";  
            }
        }

        public (JsonResult result, bool succeeded) AddandUpdateAdminUserQuestion(AdminUserSecurityQuestion securityQuestion)
        {
            try
            {
                if (securityQuestion.QuestionId <= 0)
                {
                    AdminUserSecurityQuestion adminUserSecurityQuestion = new AdminUserSecurityQuestion
                    {
                        //QuestionId = securityQuestion.QuestionId,
                        AccountId = securityQuestion.AccountId,
                        Question = securityQuestion.Question,
                        Answer = securityQuestion.Answer
                    };
                    _synchrosignContext.AdminUserSecurityQuestions.Add(adminUserSecurityQuestion);
                    _synchrosignContext.SaveChanges();
                    return (new JsonResult("data save Successfully."), true);

                }
                else
                {
                    var details = _synchrosignContext.AdminUserSecurityQuestions.Where(x => x.QuestionId == securityQuestion.QuestionId).FirstOrDefault();
                    details.Question = securityQuestion.Question;
                    details.AccountId = securityQuestion.AccountId;
                    details.Answer = securityQuestion.Answer;
                    _synchrosignContext.AdminUserSecurityQuestions.Update(details);
                    _synchrosignContext.SaveChanges();
                    return (new JsonResult("Data Update successfully"), true);
                }
            }
            catch (Exception ex)
            {
                return (new JsonResult("Some server error occured. Please try again!!"), false);

            }

        }
        public (JsonResult result, bool succeeded) GetAllAdminUserQuestion()
        {
            try
            {
                var Details = _synchrosignContext.AdminUserSecurityQuestions.ToList();
                return (new JsonResult(Details), true);

            }
            catch(Exception ex)
            {
                return (new JsonResult("Some server error occured. Please try again!!"), false);
            }
        }
        public (JsonResult result,bool succeeded) GetAdminUserQuestionbyid(int QuestionId)
        {
            try
            {
                var Details = _synchrosignContext.AdminUserSecurityQuestions.Where(x => x.QuestionId == QuestionId).FirstOrDefault();
                return (new JsonResult(Details), true);
            }
            catch(Exception ex)
            {
                return (new JsonResult("Some server error occured. Please try again!!"), false);
            }
        }
    }



}

