﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SynchrobitExchange.Areas.User.Models;
using SynchrosignNew.Areas.User.Models.Model;
using SynchrosignNew.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SynchrosignNew.Areas.User.Context
{
    public class SynchrosignContext : IdentityDbContext<UserEntity, UserRoleEntity, Guid>
    {
        public SynchrosignContext(DbContextOptions<SynchrosignContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           
            modelBuilder.Entity<GenericSetting>()
                .HasKey(c => new { c.SettingID, c.AdminAccountId, c.SettingName, c.SubSettingName });
            
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<LoginHistoryEntity> LoginHistoryEntities { get; set; }

        public DbSet<CommunicationTemplate> EmailTemplate { get; set; }
        public DbSet<GenericSetting> GenericSettings { get; set; }
        public DbSet<LoginShieldEntity> LoginShields { get; set; }
        public DbSet<UserSecurityQuestion> UserSecurityQuestions { get; set; }
        public DbSet<AdminUserSecurityQuestion> AdminUserSecurityQuestions { get; set; }

    }
}
