﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SynchrosignNew.Areas.Models.Model;
using SynchrosignNew.Areas.User.InterFace;
using SynchrosignNew.Areas.User.Models.Model;
using SynchrosignNew.Filters;
using SynchrosignNew.Identity;
using SynchrosignNew.Migrations;

namespace SynchrosignNew.Areas.User.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly UserManager<UserEntity> _userManager;
        private readonly RoleManager<UserRoleEntity> _roleManager;
        private readonly ICommonService _commonService;

        public UserController(IUserService userService, UserManager<UserEntity> userManager, RoleManager<UserRoleEntity> roleManager, ICommonService commonService)
        {
            _userService = userService;
            _userManager = userManager;
            _roleManager = roleManager;
            _commonService = commonService;
        }

        [HttpPost("Register")]
        public IActionResult Register([FromBody]UserDetails userDetails)
        {
            var (result, succeeded) = _userService.CreateUserAsync(userDetails);
            if (succeeded) return Ok(result);
            return BadRequest(result);

        }


        [HttpPost("SignIn")]
        public IActionResult SignIn([FromBody]Credentials credentials, CancellationToken ct)
        {
            credentials.IpAddress = HttpContext.Connection.RemoteIpAddress.ToString();
            if (!ModelState.IsValid) return BadRequest(new ApiError(ModelState));
            var (result, succeeded) = _userService.SignIn(credentials, HttpContext);
            if (succeeded) return Ok(result);
            return BadRequest(result);

        }

        [Authorized]
        [HttpPost("ChangePassword")]
        public IActionResult ChangePassword([FromBody]ChangePassword changePassword)
        {

            if (!ModelState.IsValid) return BadRequest(new ApiError(ModelState));
            Claim claim = HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();
            UserEntity usr = _userManager.FindByIdAsync(claim.Value).GetAwaiter().GetResult();
            string rolename = _userManager.GetRolesAsync(usr).GetAwaiter().GetResult().FirstOrDefault();
            if (rolename.ToLower() == "user")
            {
                var (result, succeeded) = _userService.ChangePassword(changePassword, usr);
                if (succeeded) return Ok(result);
                return BadRequest(result);
            }
            return Unauthorized(new JsonResult("UnAuthorized User"));
        }

        [Authorized]
        [HttpDelete("AccountDelete")]
        public IActionResult AccountDelete(string AccountNo)
        {
            if (!ModelState.IsValid) return BadRequest(new ApiError(ModelState));
            Claim claim = HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();
            var emailId = claim.Value;
            UserEntity usr = _userManager.FindByNameAsync(emailId).GetAwaiter().GetResult();
            string rolename = _userManager.GetRolesAsync(usr).GetAwaiter().GetResult().FirstOrDefault();
            if (rolename.ToLower() == "user")
            {
                var (result, succeeded) = _userService.AccountDelete(AccountNo);
                if (succeeded) return Ok(result);
                return BadRequest(result);
            }
            return Unauthorized(new JsonResult("UnAuthorized User"));
        }


        [Authorized]
        [HttpPut("UpdateUserProfile")]
        public IActionResult UpdateUserProfile([FromBody]UpdateUserDetails updateUserDetails)
        {

            if (!ModelState.IsValid) return BadRequest(new ApiError(ModelState));
            Claim claim = HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();
            UserEntity usr = _userManager.FindByIdAsync(claim.Value).GetAwaiter().GetResult();
            string rolename = _userManager.GetRolesAsync(usr).GetAwaiter().GetResult().FirstOrDefault();
            if (rolename.ToLower() == "user")
            {
                var (result, succeeded) = _userService.UpdateUserProfile(updateUserDetails);
                if (succeeded) return Ok(result);
                return BadRequest(result);
            }
            return Unauthorized(new JsonResult("UnAuthorized User"));
        }


        [HttpGet("EmailVerify")]
        public IActionResult EmailVerify(string EmailID)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiError(ModelState));
            }
            var DecrptEmail = _commonService.Decrypt(EmailID);
            var (result, succeeded) = _userService.EmailVarify(DecrptEmail);
            if (succeeded) return Ok(result);
            return BadRequest(result);

        }

        [HttpGet("ForgetPasswordSendMail")]
        public IActionResult ForgetPasswordSendMail(string EmailID)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiError(ModelState));
            }
            var (result, succeeded) = _userService.ForgetPasswordSendMail(EmailID);
            if (succeeded) return Ok(result);
            return BadRequest(result);

        }

        [HttpPost("setforgetpass")]
        public IActionResult SetForgetPass([FromBody]ResetPassword resetPassword)
        {
            if (!ModelState.IsValid) return BadRequest(new ApiError(ModelState));
            var (result, succeeded) = _userService.SetForgetPass(resetPassword);
            if (succeeded) return Ok(result);
            return BadRequest(result);
        }

        [HttpGet("ResendEmailVerify")]
        public IActionResult ResendEmailVerify(string EmailID)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiError(ModelState));
            }
            var (result, succeeded) = _userService.ResendEmailVerify(EmailID);
            if (succeeded) return Ok(result);
            return BadRequest(result);
        }


        [HttpPost("CreateLoginShield")]
        public IActionResult CreateLoginShield([FromBody] LoginShield loginShield)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(new ApiError(ModelState));
            }
            Claim claim = HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();
            var emailId = claim.Value;
            UserEntity usr = _userManager.FindByNameAsync(emailId).GetAwaiter().GetResult();
            string rolename = _userManager.GetRolesAsync(usr).GetAwaiter().GetResult().FirstOrDefault();
            loginShield.AccountNo = usr.AccountNo;
            if (rolename.ToLower() == "admin")
            {
                var (result, succeeded, error) = _userService.CreateLoginShield(loginShield, HttpContext);
                if (succeeded) return Ok(result);
                return BadRequest(new JsonResult(error));
            }
            return Unauthorized(new JsonResult("UnAuthorized User"));
        }


        [Authorized]
        [HttpGet("ChangeEmail")]
        public IActionResult ChangeEmail()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiError(ModelState));
            }
            Claim claim = HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();
             UserEntity usr = _userManager.FindByIdAsync(claim.Value).GetAwaiter().GetResult();   
            string rolename = _userManager.GetRolesAsync(usr).GetAwaiter().GetResult().FirstOrDefault();
            if (rolename.ToLower() == "user")
            {
                var (result, succeeded) = _userService.ChangeEmail(usr.Email);
                if (succeeded) return Ok(result);
                return BadRequest(new JsonResult(""));
            }
            return Unauthorized(new JsonResult("UnAuthorized User"));
        }

        [Authorized]
        [HttpGet("UpdateEmail")]
        public IActionResult UpdateEmail(string OldEmailID,string NewEmailID)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiError(ModelState));
            }
            Claim claim = HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();
            UserEntity usr = _userManager.FindByIdAsync(claim.Value).GetAwaiter().GetResult();
            string rolename = _userManager.GetRolesAsync(usr).GetAwaiter().GetResult().FirstOrDefault();
            if (rolename.ToLower() == "user")
            {
                var (result, succeeded) = _userService.UpdateEmail(usr.Email,NewEmailID);
                if (succeeded) return Ok(result);
                return BadRequest(new JsonResult(""));
            }
            return Unauthorized(new JsonResult("UnAuthorized User"));
        }
        //[HttpGet("Update2FA")]
        //public IActionResult Update2FA()
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(new ApiError(ModelState));
        //    }
        //    Claim claim = HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();
        //    var emailId = claim.Value;
        //    UserEntity usr = _userManager.FindByNameAsync(emailId).GetAwaiter().GetResult();
        //    var (result, succeeded,Error) = _userService.Update2FA(usr);
        //    if (succeeded) return Ok(result);
        //    return BadRequest(result);

        //}

       
        [HttpPost("AddUpdateUserSecurityQuestion")]
        public IActionResult AddUpdateUserSecurityQuestion([FromBody]UserSecurityQuestion userSecurityQuestion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ApiError(ModelState));

            }
            var (result, succeeded) = _userService.AddUpdateUserSecurityQuestion(userSecurityQuestion);
            if (succeeded) return Ok(result);
            return BadRequest(result);

        }


        [Authorized]
        [HttpGet("getgoogleauth")]
        public IActionResult GetGoogleCode()
        {
            if (!ModelState.IsValid) return BadRequest(new ApiError(ModelState));
            Claim claim = HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();       
            UserEntity usr = _userManager.FindByIdAsync(claim.Value).GetAwaiter().GetResult();
            string rolename = _userManager.GetRolesAsync(usr).GetAwaiter().GetResult().FirstOrDefault();
            if (rolename.ToLower() == "user")
            {
                var (result, succeeded, error) = _userService.GetGoogleQRCode(usr);
                if (succeeded) return Ok(result);
                return BadRequest(new JsonResult(""));
            }
            return Unauthorized(new JsonResult("UnAuthorized User"));         
        }


        [Authorized]
        [HttpPost("enablegoogleauth")]
        public IActionResult EnableDisableTwoFactor([FromBody] Google2Factor factor)
        {
            if (!ModelState.IsValid) return BadRequest(new ApiError(ModelState));
            Claim claim = HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();
            UserEntity usr = _userManager.FindByIdAsync(claim.Value).GetAwaiter().GetResult();
            string rolename = _userManager.GetRolesAsync(usr).GetAwaiter().GetResult().FirstOrDefault();
            if (rolename.ToLower() == "user")
            {
                var (result, succeeded) = _userService.EnableDisableTwoFactor(factor, usr.AccountNo);
                if (result) return Ok(result);
                return BadRequest(new JsonResult(succeeded));
            }
            return Unauthorized(new JsonResult("UnAuthorized User"));
        }


        [Authorized]
        [HttpPost("VerifyToken")]
        public IActionResult VerifyToken([FromBody]DetailsVerify detailsVerify)
        {
            if (!ModelState.IsValid) return BadRequest(new ApiError(ModelState));
            Claim claim = HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();
            UserEntity usr = _userManager.FindByIdAsync(claim.Value).GetAwaiter().GetResult();
            string rolename = _userManager.GetRolesAsync(usr).GetAwaiter().GetResult().FirstOrDefault();
            if (rolename.ToLower() == "user")
            {
                var (result, succeeded) = _userService.VerifyToken(detailsVerify);
                if (succeeded) return Ok(result);
                return BadRequest(new JsonResult(succeeded));
            }
            return Unauthorized(new JsonResult("UnAuthorized User"));
        }
    }
}