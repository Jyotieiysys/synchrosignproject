﻿using SynchrobitExchange.Areas.User.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SynchrosignNew.Areas.User.InterFace
{
    public interface ICommonService 
    {
        string GetRandomUserName(string Name);

        int GenerateRandomNo();

        (bool Succeeded, string Error) SendEmail(string TemplateCode, EmailInformation emailInformation);

        string Decrypt(string encryptText);

        string Encrypt(string inputText);
       





    }





}
