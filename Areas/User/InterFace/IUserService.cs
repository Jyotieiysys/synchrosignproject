﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using SynchrosignNew.Areas.Models.Model;
using SynchrosignNew.Areas.User.Models.Model;
using SynchrosignNew.Identity;

namespace SynchrosignNew.Areas.User.InterFace
{
    public interface IUserService
    {

        (JsonResult result, bool Succeeded) CreateUserAsync(UserDetails form);
        (JsonResult result, bool succeeded) SignIn(Credentials Credentials, HttpContext httpContex);

        
        (JsonResult result, bool succeeded) ChangePassword(ChangePassword password, UserEntity user);
        (JsonResult result, bool Succeeded) AccountDelete(string AccountNo);     

        (JsonResult result, bool Succeeded) UpdateUserProfile(UpdateUserDetails form);
        (JsonResult result, bool Succeeded) EmailVarify(string Email);

        (JsonResult result, bool Succeeded) ForgetPasswordSendMail(string EmailID);

        (JsonResult result, bool Succeeded) SetForgetPass(ResetPassword resetPassword);

        (JsonResult result, bool succeeded) ResendEmailVerify(string EmailID);

        (JsonResult result, bool Succeeded, string Error) CreateLoginShield(LoginShield loginShield, HttpContext httpContext);
        (JsonResult result, bool succeeded) ChangeEmail(string EmailID);
        (JsonResult result, bool succeeded) UpdateEmail(string EmailId, string NewEmailID);
        //(JsonResult result, bool succeeded, string Error) Update2FA(UserEntity user);
        (JsonResult result, bool succeeded) AddUpdateUserSecurityQuestion(UserSecurityQuestion userSecurityQuestion);
        (JsonResult result, bool Succeeded, string Error) GetGoogleQRCode(UserEntity accountId);
        (bool Succeeded, string Error) EnableDisableTwoFactor(Google2Factor factor, string accountId);
        (JsonResult result, bool succeeded) VerifyToken(DetailsVerify detailsVerify);



    }
}
