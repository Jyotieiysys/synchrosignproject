﻿using Microsoft.AspNetCore.DataProtection;
using SynchrobitExchange.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SynchrosignNew.Areas.User.Models.Model
{
    public class ChangePassword
    {
        //[Required]
        //public int Otp { get; set; }

        [Required]
        public string OldPassword { get; set; }

        [Required]
        [MinLength(8)]
        [MaxLength(20)]
        [Display(Name = "newPassword", Description = "New password")]
        [DataType(DataType.Password)]
        [RegularExpression(@"^((?!\s).)*$", ErrorMessage = "Password can not contain space.")]
        public string NewPassword { get; set; }

        [Required]
        [MinLength(8)]
        [MaxLength(20)]
        [Display(Name = "confirmPassword", Description = "Confirm password")]
        [Secret]
        [Compare("NewPassword")]
        public string ConfirmPassword { get; set; }
    }
}
