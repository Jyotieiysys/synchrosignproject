﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SynchrosignNew.Areas.Models.Model
{
    public class Credentials
    {
        [Required]
        [EmailAddress]
        [Display(Name = "UserName")]
        public string useremail { get; set;}
        public string UserName { get; set; }

        [Required]
        //[StringLength(20, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        public string MacAddress { get; set; }
        public string IpAddress { get; set; }
        public string Browser { get; set; }
        public string Browser_Version { get; set; }
        public bool Active { get; set; }
        public string UserAgent { get; set; }
        public string Location { get; set; }
        public string Os { get; set; }
        public string Os_Version { get; set; }

        public string AccountNo { get; set; }
    }
}
