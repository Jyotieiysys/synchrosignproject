﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SynchrosignNew.Areas.User.Models.Model
{
    public class DetailsVerify
    {
        public string OTP { get; set; }
        public string AccountNo { get; set; }

    }
}
