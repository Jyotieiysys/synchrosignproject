﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SynchrobitExchange.Areas.User.Models
{
    public class EmailInformation
    {
        public string MacAddress { get; set; }
        public string IpAddress { get; set; }
        public string Browser { set; get; }
        public string UserAgent { set; get; }
        public bool IsActive { get; set; }
        public string AccountNo { get; set; }
        public string EmailAddress { get; set; }
        public string Location { get; set; }
        public string UserName { get; set; }
        public string SuspendLink { get; set; }
        public string ContactLink { get; set; }
        public string WebLink { get; set; }
        public string ShieldLink { get; set; }
        public string ResetPassLink { get; set; }
        public string ActivationLink { get; set; }
        public string RemoveCCLink { get; set; }
        public string RemoveG2Factor { get; set; }

        public string Termsandcondition { get; set; }
        public string Telegram { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Medium { get; set; }
        public string Reddit { get; set; }
        public string Youtube { get; set; }
        public string Slack { get; set; }
        public string Steemit { get; set; }
        public string Cryptoamount { get; set; }
        public decimal Tokenamount { get; set;}

        public decimal MinimumWithdrawalLimit { get; set; }
        public decimal MaximumWithdrawalLimit { get; set; }
        public decimal MinimumTradingLimit { get; set; }
        public decimal MaximumTradingLimit { get; set; }

        public string Reason { get; set; }
        public string Level { get; set; }
        public string Brokerage { get; set; }
        public string TradeId { get; set; }
        public string Message { get; set; }
        public string FromEmail { get; set; }
        public string Token { get; set; }

        public string TicketDescription { get; set; }
        public string TicketStatus { get; set; }
        public string ServiceName { get; set; }
        public string WalletCode { get; set; }

    }

    public class ContactUS
    {
        //[Required(ErrorMessage ="Email is required !")]
        public string FromEmail { get; set; }

        //[Required(ErrorMessage ="Message is required !")]
        public string Message { get; set; }
        public string Name { get; set; }
    }

    public class InviteUserEmail
    {
        public List<string> UserEmail { get; set; }
    }
}
