﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SynchrosignNew.Areas.User.Models.Model
{
    public class LoginShield
    { 
        
        public string MacAddress { get; set; }
        public string IpAddress { get; set; }
        public string Browser { set; get; }
        public string UserAgent { set; get; }
        public bool IsActive { get; set; }
        public string AccountNo { get; set; }
        public string Location { get; set; }
    }
}
