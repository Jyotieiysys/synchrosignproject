﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SynchrosignNew.Areas.User.Models.Model
{
    public class LoginShieldEntity
    {
        [Key]
        public Guid ShieldId { get; set; }
        public string AccountNo { get; set; }
        public string MacAddress { get; set; }
        public string IpAddress { get; set; }
        public string Browser { get; set; }
        public bool Active { get; set; }
        public string Location { get; set; }

        public DateTimeOffset CreatedAt { get; set; }
    }
}
