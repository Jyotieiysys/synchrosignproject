﻿using SynchrobitExchange.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SynchrosignNew.Areas.User.Models.Model
{
    public class ResetPassword
    {

        [Required]
        [Display(Name = "useremail", Description = "User email")]
        [RegularExpression(@"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", ErrorMessage = "Invalid Email Address")]
        public string Useremail { get; set; }

        [Required]
        [MinLength(8)]
        [MaxLength(20)]
        [Display(Name = "userPassword", Description = "User password")]
        [RegularExpression(@"^((?!\s).)*$", ErrorMessage = "Password can not contain space.")]
        [DataType(DataType.Password)]
        [Secret]
        public string UserPassword { get; set; }

        [Required]
        [MinLength(8)]
        [MaxLength(20)]
        [Display(Name = "confirmPassword", Description = "Confirm password")]
        [Secret]
        [Compare("UserPassword")]
        public string ConfirmPassword { get; set; }

        [Required]
        public string UserToken { get; set; }

    }
}
