﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SynchrosignNew.Areas.User.Models.Model
{
    public class URLModel
    {
        public string FileName { get; set; }
        public string ImageURL{ get; set; }

    }
}
