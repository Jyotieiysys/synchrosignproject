﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SynchrosignNew.Areas.User.Models.Model
{
    public class UpdateUserDetails
    {
        public string Id { get; set; }
        public string UserName { get; set; }

        public string FullName { get; set; }

        public string PhoneNumber { get; set; }

        public string Nationality { get; set; }

        public DateTime ModifiedDate { get; set; }

       




    }
}
