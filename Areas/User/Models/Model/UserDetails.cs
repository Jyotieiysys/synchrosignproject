﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SynchrosignNew.Areas.Models.Model
{
    public class UserDetails
    {
        [Required]
        [Display(Name = "emailID", Description = "Email")]
        [EmailAddress]
        public string EmailID { get; set; }

        [Required]
        [MinLength(8)]
        [MaxLength(20)]
        [Display(Name = "password", Description = "Password")]
        [DataType(DataType.Password)]
        //^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@+|$!%*#?&.]{8,20}$
        // [RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@+|{}[?/<:>=`_~$!%*#?&.]{8,30}$", ErrorMessage = "Password can contain Alphanumeric and Special character ($@$!%*#?&) only.")]
        public string Password { get; set; }

        [Required]
        [MinLength(8)]
        [MaxLength(20)]
        [Display(Name = "confirmPassword", Description = "Confirm password")]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "fullname", Description = "Full Name")]
        public string FullName { get; set; }



        [Display(Name = "userName", Description = "User Name")]
        public string UserName { get; set; }


        //[Display(Name = "dob", Description = "DOB")]
        //public DateTime? DOB { get; set; }



        [Display(Name = "city", Description = "City")]
        public string City { get; set; }



        //[Display(Name = "district", Description = "District")]
        //public string District { get; set; }


        //[Display(Name = "state", Description = "State")]
        //public string State { get; set; }



        [Display(Name = "country", Description = "Country")]
        public string Country { get; set; }
        [Required]

        [Display(Name = "nationality", Description = "Nationality")]
        public string Nationality { get; set; }



        [Display(Name = "contactNo", Description = "Mobile No")]
        public string ContactNo { get; set; }


        [Display(Name = "macAddress", Description = "MacAddress")]
        public string MacAddress { get; set; }

        [Display(Name = "ipAddress", Description = "IpAddress")]
        public string IpAddress { get; set; }

        [Display(Name = "browser", Description = "Browser")]
        public string Browser { get; set; }

        [Display(Name = "userAgent", Description = "UserAgent")]
        public string UserAgent { get; set; }
        public string Sponsor { get; set; }
       
        public string AccountNo { get; set; }
        public string Status { get; set; }

        public string IsDeleted { get; set; }

        //public string NormalizedEmail { get; set; }

    }
}

