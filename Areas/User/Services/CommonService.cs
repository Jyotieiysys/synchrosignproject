﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using SynchrobitExchange.Areas.User.Models;
using SynchrosignNew.Areas.User.Context;
using SynchrosignNew.Areas.User.InterFace;
using SynchrosignNew.Areas.User.Models.Model;
using SynchrosignNew.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SynchrosignNew.Areas.User.Services
{
    public class CommonService : ICommonService
    {
        private readonly SynchrosignContext _synchrosignContext;
        private readonly UserManager<UserEntity> _userManager;
        IConfiguration _configuration;
        public CommonService(SynchrosignContext synchrosignContext, UserManager<UserEntity> userManager, IConfiguration configuration)
        {
            _synchrosignContext = synchrosignContext;
            _userManager = userManager;
            _configuration = configuration;
        }


        public string GetRandomUserName(string Name)
        {
            string name = Regex.Replace(Name, @"\s", "") + new Random().Next(1000, 9999).ToString();
            while (_synchrosignContext.Users.Where(s => s.UserName == name).Count() > 0)
            {
                GetRandomUserName(name);
            }
            return name;
        }

        public int GenerateRandomNo()
        {
            int _min = 100000000;
            int _max = 999999999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }

        public (bool Succeeded, string Error) SendEmail(string TemplateCode, EmailInformation emailInformation)
        {

            bool flag = false;
            string error = "";

            try
            {
                if (TemplateCode != "")
                {
                   
                    var result =_synchrosignContext.EmailTemplate.Where(s => s.TemplateCode == TemplateCode).First<CommunicationTemplate>();
                    if (result.TemplateCode != null)
                    {

                        error = SendEmail(result, emailInformation);
                        if (error == "Success") { flag = true; }
                    }
                }
                return (flag, error);
            }
            catch (Exception Ex)
            {
                return (flag, Ex.Message);
            }
        }

        public string SendEmail(CommunicationTemplate result, EmailInformation emailInformation)
        {
            string error = "";
            string body = result.Body;

            var usr = _synchrosignContext.Users.Where(x => (x.Email== emailInformation.EmailAddress)).FirstOrDefault();
            if (usr != null)
            {
                if (usr.FullName.Contains(" "))
                {
                    var commands = usr.FullName.Split(' ', 2);
                    usr.FullName = commands[0];
                }
            }
            try
            {
                if (body.IndexOf("#content") > 0)
                {
                    body = body.Replace("#content", usr.FullName);
                }
                if (body.IndexOf("#ActivationLink#") > 0)
                {
                    body = body.Replace("#ActivationLink#", emailInformation.ActivationLink);
                }
                if(body.IndexOf("#User#")>0)
                {
                    body = body.Replace("#User#", usr.FullName);
                }
                if(body.IndexOf("#ResetPassLink#") >0)
                {
                    body = body.Replace("#ResetPassLink#", emailInformation.ResetPassLink);
                }
                if(body.IndexOf(" #IPADDRESS#")>0)
                {
                    body = body.Replace("#IPADDRESS#", emailInformation.IpAddress);
                }
                if (body.IndexOf("#Country#") > 0)
                {
                    body = body.Replace("#Country#", emailInformation.Location);
                }
                if (body.IndexOf("#Browser#") > 0)
                {
                    body = body.Replace("#Browser#", emailInformation.Browser);
                }
                if (body.IndexOf("#Agent#") > 0)
                {
                    body = body.Replace("#Agent#", emailInformation.Browser);
                }
                if (body.IndexOf("#DateTime#") > 0)
                {
                    body = body.Replace("#DateTime#", DateTime.Now.ToString());
                }
                if (body.IndexOf("#ShieldLink#") > 0)
                {
                    body = body.Replace("#ShieldLink#", emailInformation.ShieldLink);
                }
                SmtpClient smtp = new SmtpClient
                {
                    Host = result.HostName,
                    Port = result.HostPort,
                    EnableSsl = false,
                    Credentials = new System.Net.NetworkCredential(result.SMTPUsername, result.SMTPPassword),

                };
                MailMessage message = new MailMessage(result.SenderEmail, emailInformation.EmailAddress, result.Subject, body);
                message.From = new MailAddress(result.SenderEmail);
                message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess;
                message.IsBodyHtml = true;

                smtp.Send(message);
                error = "Success";

                string status = "";// message.DeliveryNotificationOptions.ToString();
                if (status == "OnSuccess")
                { emailInformation.IsActive = true; }
                
            }
            catch (Exception Ex)
            {
                error = Ex.ToString();
                //_logService.writeLog(_env.WebRootPath, Ex.ToString());
                //   _adminCommonService.ReportServiceIssue((Int32)Enums.ServicetypeID.EmailID);
            }
            return error;
        }

        public string Decrypt(string encryptText)
        {
            try
            {
                string encryptionkey = _configuration.GetSection("Encryption:Key").Value;
                byte[] keybytes = Encoding.ASCII.GetBytes(encryptionkey.Length.ToString());
                RijndaelManaged rijndaelCipher = new RijndaelManaged();
                byte[] encryptedData = Convert.FromBase64String(encryptText.Replace(" ", "+"));
                PasswordDeriveBytes pwdbytes = new PasswordDeriveBytes(encryptionkey, keybytes);
                using (ICryptoTransform decryptrans = rijndaelCipher.CreateDecryptor(pwdbytes.GetBytes(32), pwdbytes.GetBytes(16)))
                {
                    using (MemoryStream mstrm = new MemoryStream(encryptedData))
                    {
                        using (CryptoStream cryptstm = new CryptoStream(mstrm, decryptrans, CryptoStreamMode.Read))
                        {
                            byte[] plainText = new byte[encryptedData.Length];
                            int decryptedCount = cryptstm.Read(plainText, 0, plainText.Length);
                            return Encoding.Unicode.GetString(plainText, 0, decryptedCount);
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                
                return Ex.Message;
            }
        }

        public string Encrypt(string inputText)
        {
            try
            {
                string encryptionkey = _configuration.GetSection("Encryption:Key").Value;
                byte[] keybytes = Encoding.ASCII.GetBytes(encryptionkey.Length.ToString());
                RijndaelManaged rijndaelCipher = new RijndaelManaged();
                byte[] plainText = Encoding.Unicode.GetBytes(inputText);
                PasswordDeriveBytes pwdbytes = new PasswordDeriveBytes(encryptionkey, keybytes);
                using (ICryptoTransform encryptrans = rijndaelCipher.CreateEncryptor(pwdbytes.GetBytes(32), pwdbytes.GetBytes(16)))
                {
                    using (MemoryStream mstrm = new MemoryStream())
                    {
                        using (CryptoStream cryptstm = new CryptoStream(mstrm, encryptrans, CryptoStreamMode.Write))
                        {
                            cryptstm.Write(plainText, 0, plainText.Length);
                            cryptstm.Close();
                            return Convert.ToBase64String(mstrm.ToArray());
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
               
                return Ex.Message;
            }
        }
    }

}
