﻿using Amazon.S3;
using Amazon.S3.Transfer;
using Amazon.S3.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using SynchrobitExchange.Areas.User.Models;
using SynchrosignNew.Areas.Models.Model;
using SynchrosignNew.Areas.User.Context;
using SynchrosignNew.Areas.User.InterFace;
using SynchrosignNew.Areas.User.Models.Model;
using SynchrosignNew.Identity;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using SynchrosignNew.Admin.Models.Model;
using System.IO;
using System.Web;
using Google.Authenticator;
using SynchrosignNew.Service;
using Microsoft.Extensions.Options;

namespace SynchrosignNew.Areas.User.Services
{
    public class UserService : IUserService
    {

        private readonly SynchrosignContext _synchrosignContext;
        private readonly ICommonService _commonService;
        private readonly UserManager<UserEntity> _userManager;
        private readonly SignInManager<UserEntity> _signInManager;
        IConfiguration _configuration;
        private readonly JwtIssuerOptions _jwtOptions;


        public UserService(SynchrosignContext synchrosignContext, ICommonService commonService, UserManager<UserEntity> userManager,
            SignInManager<UserEntity> signInManager, IConfiguration configuration, IOptions<JwtIssuerOptions> jwtOptions)
        {
            _synchrosignContext = synchrosignContext;
            _commonService = commonService;
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _jwtOptions = jwtOptions.Value;
        }


        public (JsonResult result, bool Succeeded) CreateUserAsync(UserDetails form)
        {
            using (var dbContextTransaction = _synchrosignContext.Database.BeginTransaction())
            {
                try
                {

                    #region UserCreation


                    string username = _commonService.GetRandomUserName(form.FullName);
                    var customer = _commonService.GenerateRandomNo().ToString();
                    if (_synchrosignContext.Users.Where(x => x.Id.ToString() == customer).FirstOrDefault() != null)
                    {
                        customer = _commonService.GenerateRandomNo().ToString();
                    }
                    var entity = new UserEntity
                    {
                        //UserId = Guid.NewGuid(),
                        Email = form.EmailID,
                        UserName = form.UserName,
                        FullName = form.FullName,
                        Password = form.Password,
                        SelectNationality = form.Nationality,
                        NormalizedEmail = form.EmailID,
                        AccountNo = form.AccountNo,
                        Status = "",
                        IsDeleted = "No",

                        CreatedDate = DateTime.Now

                    };

                    Claim[] claims = new Claim[]
                         {
                           new Claim(ClaimTypes.NameIdentifier, entity.Email),
                           new Claim(JwtRegisteredClaimNames.Sub, entity.Email)
                         };
                    var result = _userManager.CreateAsync(entity, form.Password).GetAwaiter().GetResult();


                    if (!result.Succeeded)
                    {
                        var firstError = result.Errors.FirstOrDefault()?.Description;
                        string rmstring = (firstError.Substring(0, firstError.IndexOf(@"'")) + firstError.Substring(firstError.LastIndexOf("'") + 1));
                        return (new JsonResult(rmstring), result.Succeeded);
                    }
                    else
                    {
                        _userManager.AddToRoleAsync(entity, "Admin").Wait();
                        _userManager.AddClaimsAsync(entity, claims).Wait();
                        _userManager.UpdateAsync(entity).Wait();
                        _synchrosignContext.SaveChanges();

                        EmailInformation emailInformation = new EmailInformation();
                        emailInformation.EmailAddress = form.EmailID;
                        var encEmailId = _commonService.Encrypt(form.EmailID);
                        emailInformation.ActivationLink = "https://localhost:44343/api/User/EmailVerify?EmailID=" + encEmailId;

                        _commonService.SendEmail("ACCACT", emailInformation);
                    }



                    dbContextTransaction.Commit();
                    return (new JsonResult("User Created Successfully."), result.Succeeded);
                    #endregion

                }
                catch (Exception Ex)
                {
                    dbContextTransaction.Rollback();
                    return (new JsonResult("Some server error occured. Please try again!!"), false);
                }

            }



        }

        public (JsonResult result, bool succeeded) SignIn(Credentials Credentials, HttpContext httpContex)
        {
            try
            {

                Credentials.IpAddress = httpContex.Connection.RemoteIpAddress.ToString();
                if (Credentials.IpAddress.Contains("ffff")) Credentials.IpAddress = Credentials.IpAddress.Substring(7);
                var useremail = _userManager.FindByEmailAsync(Credentials.useremail);

                if (useremail.Result != null)
                {
                    if (useremail.Result.IPAddress == httpContex.Connection.RemoteIpAddress.ToString())
                    {

                        if (useremail.Result.Status == "Active")
                        {

                            var result = _signInManager.PasswordSignInAsync(useremail.Result.UserName, Credentials.Password, false, false).GetAwaiter().GetResult();

                            TwoFactorAuthenticator twoFactorAuthenticator = new TwoFactorAuthenticator();
                            var setupInfo = twoFactorAuthenticator.GenerateSetupCode(useremail.Result.Email, useremail.Result.AccountNo, 250, 250);
                            string qrURL = setupInfo.QrCodeSetupImageUrl;

                            if (result.Succeeded)
                            {
                                Credentials.AccountNo = useremail.Result.AccountNo;
                                Credentials.UserName = useremail.Result.UserName;

                                //if (_userManager.GetTwoFactorEnabledAsync(useremail.Result).GetAwaiter().GetResult())
                                //{
                                //    return (new JsonResult(new Dictionary<string, object>
                                //    {
                                //         { "EncreptedACC", _commonService.Encrypt(useremail.Result.AccountNo) },
                                //           { "Twofactor", useremail.Result.TwoFactorEnabled },
                                //            { "Message", "Google 2FA enabled" },
                                //            { "QRURL", qrURL }
                                //    }), useremail.Result.TwoFactorEnabled);
                                //}


                                var user = _userManager.FindByNameAsync(useremail.Result.UserName).GetAwaiter().GetResult();

                                if (user.LoginShied)
                                {
                                    LoginShield loginShield = new LoginShield
                                    {
                                        AccountNo = user.AccountNo,
                                        Browser = Credentials.Browser,
                                        IpAddress = Credentials.IpAddress,
                                        IsActive = false,
                                        Location = Credentials.Location,
                                        MacAddress = Credentials.MacAddress,
                                        UserAgent = Credentials.UserAgent
                                    };
                                    var LoginShieldResult = CreateLoginShield(loginShield, httpContex);
                                    //if (LoginShieldResult.Succeeded)
                                    //{
                                    //    return (new JsonResult(new Dictionary<string, string>{
                                    //                            { "Email", user.Email },
                                    //                            { "LoginShield", "true" },
                                    //                            { "Message", "An email has been sent to your registered email id as you login using different IP. Please verify to login." }
                                    //            }), true);
                                    //}
                                }

                                var loginHtory = LoginHistory(Credentials);
                                var Token = GenerateToken(user, Credentials.IpAddress);
                                return (Token, result.Succeeded);

                            }
                            else
                            {
                                return (new JsonResult("Authentication failed!! Please check credentials.") { StatusCode = 401 }, false);
                            }
                        }
                        else
                        {
                            return (new JsonResult("User is not Active."), false);
                        }
                    }
                    else
                    {
                        Guid guid = Guid.NewGuid();
                        string ShieldLink = (Convert.ToBoolean(_configuration.GetSection("Environment:Staging").Value) ? _configuration.GetSection("exchangeUrl:DevShieldLink").Value.ToString() : _configuration.GetSection("exchangeUrl:ShieldLink").Value.ToString());
                        ShieldLink += HttpUtility.UrlEncode(_commonService.Encrypt(guid.ToString()));
                        EmailInformation emailInformation = new EmailInformation
                        {
                            //Browser = httpContex.Connection.Browser,
                            //IpAddress = loginShield.IpAddress,
                            //Location = loginShield.Location,
                            //MacAddress = loginShield.MacAddress,
                            //UserAgent = loginShield.UserAgent,
                            EmailAddress = useremail.Result.Email,
                            AccountNo = useremail.Result.AccountNo,
                            UserName = useremail.Result.UserName,
                            ShieldLink = ShieldLink
                        };
                        var (succeeded, error) = _commonService.SendEmail("SL", emailInformation);
                        return (new JsonResult(""), true);
                    }
                }
                else
                {
                    return (new JsonResult("Signin is not possible Right Now!!") { StatusCode = 401 }, false);
                }
            }

            catch (Exception ex)
            {
                return (new JsonResult("Some server error has occurred. Please try again!!"), false);
            }
        }
        private JsonResult GenerateToken(UserEntity user, string IPAddress)
        {

            var now = DateTime.UtcNow;
            var claims = new Claim[]
            {
                //new Claim("Email", user.Email), 
                new Claim("username", user.UserName),
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, new DateTimeOffset(now).ToUniversalTime().ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64)
            };
            //var secretKey = _configuration.GetSection("TokenAuthentication:SecretKey").Value;
            //var issuer = _configuration.GetSection("TokenAuthentication:Issuer").Value;
            //var audience = _configuration.GetSection("TokenAuthentication:Audience").Value;
            //var signingKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
            //// Create the JWT and write it to a string
            //var jwt = new JwtSecurityToken(
            //    issuer: issuer,
            //    audience: audience,
            //    claims: claims,
            //    notBefore: now,
            //    expires: now.Add(TimeSpan.FromMinutes(120)),
            //    signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256));
            //var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var jwt = new JwtSecurityToken(
           issuer: _jwtOptions.Issuer,
           audience: _jwtOptions.Audience,
           claims: claims,
           notBefore: _jwtOptions.NotBefore,
           expires: _jwtOptions.Expiration,
           signingCredentials: _jwtOptions.SigningCredentials);
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new JsonResult(new Dictionary<string, object>
            {
                { "access_token" , encodedJwt },
                { "expires_in" , (int) TimeSpan.FromMinutes(200).TotalSeconds },
                {"user_name",user.UserName },
                {"fullName",user.FullName },
                {"IPAddress", IPAddress },
                 {"LoginShield",user.LoginShied },
                 {"AccountNo",user.AccountNo },
                {"Message", "Login Successfully" }

            });

            return response;
        }
        public (JsonResult result, bool succeeded) ChangePassword(ChangePassword password, UserEntity user)
        {
            try
            {

                var userdetails = _synchrosignContext.Users.Where(x => x.Id == user.Id).FirstOrDefault();
                if (password.OldPassword == password.NewPassword)
                {
                    return (new JsonResult("Old password and new password can't be same."), false);
                }

                var result = _userManager.ChangePasswordAsync(user, password.OldPassword, password.NewPassword).GetAwaiter().GetResult();

                if (result.Succeeded)
                {
                    return (new JsonResult("Password has changed successfully. Please login with new password."), true);
                }
                return (new JsonResult("Some server error has occurred. Please try again!!"), false);
            }
            catch (Exception ex)
            {
                return (new JsonResult("Some server error has occurred. Please try again!!"), false);
            }
        }

        public (JsonResult result, bool Succeeded) AccountDelete(string AccountNo)
        {
            try
            {
                var user = _userManager.Users.Where(u => u.AccountNo == AccountNo).First();
                user.IsDeleted = "yes";
                var result = _userManager.UpdateAsync(user).GetAwaiter().GetResult();
                if (result.Succeeded)
                {
                    EmailInformation emailInformation = new EmailInformation();
                    emailInformation.EmailAddress = user.Email;
                    var (Succeeded, Error) = _commonService.SendEmail("DELETEACCOUNT", emailInformation);

                    return (new JsonResult("User account deleted successfully."), true);
                }
                else
                {
                    return (new JsonResult(result.Errors), false);
                }
            }
            catch (Exception ex)
            {
                return (new JsonResult("Some server error has occurred. Please try again!!"), false);
            }

        }

        public (JsonResult result, bool Succeeded) LoginHistory(Credentials credentials)
        {
            try
            {
                LoginHistoryEntity loginHistroryEntity = new LoginHistoryEntity()
                {
                    LoginHistoryID = Guid.NewGuid(),
                    UserName = credentials.UserName,
                    AccountNo = credentials.AccountNo,
                    LogInTime = DateTime.Now,
                    Email = credentials.useremail
                };
                _synchrosignContext.LoginHistoryEntities.Add(loginHistroryEntity);
                _synchrosignContext.SaveChangesAsync();
                return (new JsonResult("History created successfully."), true);

            }
            catch (Exception ex)
            {
                return (new JsonResult("Some server error has occurred. Please try again!!"), false);
            }
        }

        public (JsonResult result, bool Succeeded) UpdateUserProfile(UpdateUserDetails form)
        {
            string message = "Profile details Updated.";
            try
            {
                var user = _userManager.FindByIdAsync(form.Id).GetAwaiter().GetResult();
                if (user != null)
                {
                    user.FullName = form.FullName;
                    user.ModifiedDate = DateTime.Now;
                    user.SelectNationality = form.Nationality;
                    user.UserName = form.UserName;
                    user.PhoneNumber = form.PhoneNumber;
                }
                var res = _userManager.UpdateAsync(user).GetAwaiter().GetResult();
                if (res.Succeeded)
                {
                    return (new JsonResult(message), true);
                }
                else
                {
                    return (new JsonResult(res.Errors.FirstOrDefault()?.Description), false);
                }
            }
            catch (Exception ex)
            {
                return (new JsonResult("Some server error occured. Please try again!!"), false);
            }

        }

        public (JsonResult result, bool Succeeded) EmailVarify(string Email)
        {
            try
            {
                var user = _userManager.Users.Where(u => u.Email == Email).First();
                user.EmailConfirmed = true;
                user.Status = "Active";
                var result = _userManager.UpdateAsync(user).GetAwaiter().GetResult();

                return (new JsonResult("Email Verify Succesfully."), true);
            }
            catch (Exception ex)
            {
                return (new JsonResult("Some server error occured. Please try again!!"), false);
            }
        }

        public (JsonResult result, bool Succeeded) ForgetPasswordSendMail(string EmailID)
        {
            try
            {
                var user1 = _userManager.Users.Where(u => u.Email == EmailID).First();
                if (user1 != null)
                {
                    _userManager.Options.Tokens.PasswordResetTokenProvider = "UserDefault";
                    var token = _userManager.GeneratePasswordResetTokenAsync(user1);
                    if (token.Result != null || token.Result != "")
                    {
                        string forgetpasswordlink = (Convert.ToBoolean(_configuration.GetSection("Environment:Staging").Value) ? _configuration.GetSection("exchangeUrl:DevResetLink").Value.ToString() : _configuration.GetSection("exchangeUrl:ResetLink").Value.ToString());
                        forgetpasswordlink += user1.Email + "&token=" + token.Result;

                        EmailInformation emailInformation = new EmailInformation
                        {
                            EmailAddress = EmailID,
                            ResetPassLink = forgetpasswordlink,
                            UserName = user1.UserName,
                            AccountNo = user1.AccountNo
                        };
                        var returnEmail = _commonService.SendEmail("RESPAS", emailInformation);

                        return (new JsonResult("Mail Send Successfully."), true);
                    }
                    else
                    {
                        return (new JsonResult("Some server error has occurred. Please try again!!"), false);
                    }
                }
                else
                {
                    return (new JsonResult("Invalid User Account "), true);
                }

            }
            catch (Exception ex)
            {
                return (new JsonResult("Some server error occured. Please try again!!"), false);
            }

        }
        public (JsonResult result, bool Succeeded) SetForgetPass(ResetPassword resetPassword)
        {
            try
            {
                var user2 = _userManager.FindByEmailAsync(resetPassword.Useremail).GetAwaiter().GetResult();
                if (user2 != null)
                {
                    var changepass = _userManager.ResetPasswordAsync(user2, resetPassword.UserToken, resetPassword.UserPassword).GetAwaiter().GetResult();
                    if (changepass.Succeeded)
                    {
                        EmailInformation emailInformation = new EmailInformation
                        {
                            EmailAddress = resetPassword.Useremail,
                            UserName = user2.UserName
                        };
                        var (Succeeded, Error) = _commonService.SendEmail("PASCNG", emailInformation);
                        return (new JsonResult("Password has been changed successfully."), true);
                    }
                    else
                        return (new JsonResult("Reset password link has expired"), false);

                }
                else
                    return (new JsonResult("Reset password link has expired"), false);


            }
            catch (Exception ex)
            {
                return (new JsonResult("Some server error has occurred. Please try again!!"), false);
            }
        }

        public (JsonResult result, bool succeeded) ResendEmailVerify(string EmailID)
        {
            try
            {
                var details = _userManager.Users.Where(x => x.Email == EmailID).FirstOrDefault();
                if (details != null)
                {
                    var encEmailId = _commonService.Encrypt(EmailID);
                    string Activationlink = (Convert.ToBoolean(_configuration.GetSection("Environment:Staging").Value) ? _configuration.GetSection("exchangeUrl:DevActivationLink").Value.ToString() : _configuration.GetSection("exchangeUrl:ActivationLink").Value.ToString());
                    Activationlink = Activationlink + encEmailId;

                    EmailInformation emailInformation = new EmailInformation();
                    {
                        emailInformation.EmailAddress = details.Email;
                        emailInformation.ActivationLink = Activationlink;
                    }
                    var returnEmail = _commonService.SendEmail("RESEND", emailInformation);
                    return (new JsonResult("Mail Send Successfully."), returnEmail.Succeeded);
                }
                else
                {
                    return (new JsonResult("Invalid User Details."), false);
                }

            }
            catch (Exception ex)
            {
                return (new JsonResult("Some server error has occurred. Please try again!!"), false);
            }

        }

        public (JsonResult result, bool Succeeded, string Error) CreateLoginShield(LoginShield loginShield, HttpContext httpContext)
        {
            var Emailerror = "";
            UserEntity userEntity = (UserEntity)_synchrosignContext.Users.Where(x => x.AccountNo == loginShield.AccountNo).FirstOrDefault<UserEntity>();
            string IPAddress = httpContext.Connection.RemoteIpAddress.ToString();
            if (IPAddress.Contains("ffff")) IPAddress = IPAddress.Substring(7);
            Guid guid = Guid.NewGuid();
            try
            {
                if (_synchrosignContext.LoginShields.Where(x => x.IpAddress == loginShield.IpAddress && x.AccountNo == loginShield.AccountNo).Select(r => new { r.AccountNo, r.Active }).Any())
                {
                    var Details = _synchrosignContext.LoginShields.Where(x => x.IpAddress == loginShield.IpAddress && x.AccountNo == loginShield.AccountNo).First();
                    if (!Details.Active)
                    {
                        string ShieldLink = (Convert.ToBoolean(_configuration.GetSection("Environment:Staging").Value) ? _configuration.GetSection("exchangeUrl:DevShieldLink").Value.ToString() : _configuration.GetSection("exchangeUrl:ShieldLink").Value.ToString());
                        ShieldLink += HttpUtility.UrlEncode(_commonService.Encrypt(guid.ToString()));
                        EmailInformation emailInformation = new EmailInformation
                        {
                            Browser = loginShield.Browser,
                            IpAddress = loginShield.IpAddress,
                            Location = loginShield.Location,
                            MacAddress = loginShield.MacAddress,
                            UserAgent = loginShield.UserAgent,
                            EmailAddress = userEntity.Email,
                            AccountNo = userEntity.AccountNo,
                            UserName = userEntity.UserName,
                            ShieldLink = ShieldLink
                        };
                        var (succeeded, error) = _commonService.SendEmail("SL", emailInformation);
                        if (!succeeded)
                        {
                            return (new JsonResult("Invalid Data"), false, error);
                        }
                        return (new JsonResult("Mail Send Successful"), true, "");
                    }
                    else
                    {
                        return (new JsonResult("Ivalid data"), true, "");
                    }
                }
                else
                {
                    return (new JsonResult("Ivalid data"), true, "");
                }
            }
            catch (Exception ex)
            {
                return (new JsonResult("Some server error has occurred. Please try again!!"), false, "");
            }
        }
        public (JsonResult result, bool succeeded) ChangeEmail(string EmailID)
        {
            try
            {
                Guid guid = Guid.NewGuid();
                var user1 = _userManager.Users.Where(u => u.Email == EmailID).First();
                if (user1 != null)
                {
                    string ShieldLink = (Convert.ToBoolean(_configuration.GetSection("Environment:Staging").Value) ? _configuration.GetSection("exchangeUrl:DevShieldLink").Value.ToString() : _configuration.GetSection("exchangeUrl:ShieldLink").Value.ToString());
                    ShieldLink += HttpUtility.UrlEncode(_commonService.Encrypt(guid.ToString()));
                    EmailInformation emailInformation = new EmailInformation
                    {
                        EmailAddress = user1.Email,
                        AccountNo = user1.AccountNo,
                        UserName = user1.UserName,
                        ShieldLink = ShieldLink
                    };

                    var returnEmail = _commonService.SendEmail("CHANGEEMAIL", emailInformation);
                    return (new JsonResult("Mail Send Successfully."), returnEmail.Succeeded);
                }
                return (new JsonResult("Invalid Details."), false);
            }
            catch (Exception ex)
            {
                return (new JsonResult("Some server error has occurred. Please try again!!"), false);
            }
        }
        public (JsonResult result, bool succeeded) UpdateEmail(string EmailId, string NewEmailID)
        {
            try
            {
                var user = _userManager.Users.Where(x => x.Email == EmailId).FirstOrDefault();
                user.Email = NewEmailID;
                var result = _userManager.UpdateAsync(user).GetAwaiter().GetResult();
                if (result.Succeeded)
                {
                    EmailInformation emailInformation = new EmailInformation
                    {
                        EmailAddress = user.Email,
                        UserName = user.UserName,
                        AccountNo = user.AccountNo,
                    };

                    var details = _commonService.SendEmail("UPDATEEMAIL", emailInformation);
                    return (new JsonResult(" Your Mail has been Updated."), details.Succeeded);
                }
                return (new JsonResult("Invalid Details"), false);

            }
            catch (Exception ex)
            {
                return (new JsonResult("Some server error has occurred. Please try again!!"), false);
            }
        }
        //public (JsonResult result, bool succeeded, string Error) Update2FA(UserEntity user)
        //{
        //    try
        //    {
        //        var Details = _synchrosignContext.Users.Where(x => x.Id == user.Id).FirstOrDefault();
        //        Details.TwoFactorEnabled = Details.TwoFactorEnabled == true ? false : true;
        //        _synchrosignContext.Users.Update(Details);
        //        _synchrosignContext.SaveChanges();
        //        if (Details.TwoFactorEnabled)
        //        {
        //            return (new JsonResult("2FA is activated successfully."), true, "");
        //        }
        //        else
        //        {
        //            return (new JsonResult("2FA is de-activated successfully."), true, "");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return (new JsonResult(""), false, ex.Message);
        //    }
        //}

        public (JsonResult result, bool succeeded) AddUpdateUserSecurityQuestion(UserSecurityQuestion userSecurityQuestion)
        {
            try
            {
                if (userSecurityQuestion.QuestionId <= 0)
                {
                    UserSecurityQuestion userSecurityQuestion1 = new UserSecurityQuestion
                    {
                        AccountId = userSecurityQuestion.AccountId,
                        Question = userSecurityQuestion.Question,
                        Answer = userSecurityQuestion.Answer,
                        UserId = userSecurityQuestion.UserId,
                    };
                    _synchrosignContext.UserSecurityQuestions.Add(userSecurityQuestion1);
                    _synchrosignContext.SaveChanges();
                    return (new JsonResult("data save Successfully."), true);


                }
                else
                {
                    var Details = _synchrosignContext.UserSecurityQuestions.Where(x => x.QuestionId == userSecurityQuestion.QuestionId).FirstOrDefault();
                    Details.Question = userSecurityQuestion.Question;
                    Details.Answer = userSecurityQuestion.Answer;
                    Details.AccountId = userSecurityQuestion.AccountId;
                    Details.UserId = userSecurityQuestion.UserId;
                    _synchrosignContext.UserSecurityQuestions.Update(Details);
                    _synchrosignContext.SaveChanges();
                    return (new JsonResult("Data Update successfully"), true);

                }
            }
            catch (Exception ex)
            {
                return (new JsonResult("Some server error occured. Please try again!!"), false);
            }
        }
        public (JsonResult result, bool Succeeded, string Error) GetGoogleQRCode(UserEntity accountId)
        {
            try
            {

                TwoFactorAuthenticator twoFactorAuthenticator = new TwoFactorAuthenticator();
                var setupInfo = twoFactorAuthenticator.GenerateSetupCode(accountId.Email, accountId.AccountNo, 250, 250);
                string qrURL = setupInfo.QrCodeSetupImageUrl;
                string PrivateCode = setupInfo.ManualEntryKey;

                return (new JsonResult(new Dictionary<string, object> {
                    {"QrCodeURL",qrURL },
                    { "PrivateCode",PrivateCode}
                   }),
                        true, ""
                       );
            }
            catch (Exception Ex)
            {

                return (new JsonResult(""), true, Ex.Message);
            }
        }
        public (bool Succeeded, string Error) EnableDisableTwoFactor(Google2Factor factor, string accountId)
        {
            try
            {
                var result = _synchrosignContext.Users.Where(s => s.AccountNo == accountId);
                if (result.Count() > 0)
                {
                    TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                    bool isCorrectPIN = tfa.ValidateTwoFactorPIN(accountId, factor.opt, TimeSpan.FromSeconds(15));
                    if (isCorrectPIN)
                    {
                        result.FirstOrDefault().TwoFactorEnabled = factor.loginbool;
                        //result.FirstOrDefault().TwoFactorTrans = factor.transbool;
                        _synchrosignContext.SaveChanges();
                        return (true, "");
                    }

                }
                return (false, "Google cannot verify your code. Please try again.");
            }
            catch (Exception Ex)
            {

                return (false, Ex.Message);
            }
        }

        public (JsonResult result, bool succeeded) VerifyToken(DetailsVerify detailsVerify)
        {
            try
            {

                var decrptAccountNo = _commonService.Decrypt(detailsVerify.AccountNo);
                TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                bool isCorrectPIN = tfa.ValidateTwoFactorPIN(decrptAccountNo, detailsVerify.OTP, TimeSpan.FromSeconds(15));
                if (isCorrectPIN)
                {
                    var user = _synchrosignContext.Users.Where(x => x.AccountNo == decrptAccountNo).FirstOrDefault();
                    var Token = GenerateToken(user, user.IPAddress);
                    return (new JsonResult(Token), true);
                }
                else
                {
                    return (new JsonResult("Invalid OTP"), false);
                }


            }
            catch (Exception ex)
            {
                return (new JsonResult("Some server error occured. Please try again!!"), false);
            }
        }

    }



}
