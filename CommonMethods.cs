﻿using System;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using SynchrosignNew.Models;

namespace SynchrosignNew.Common
{
    public static class CommonMethods
    {
        public static TokenModel GetTokenDataModel(HttpContext request)
        {
            // TokenModel token = new TokenModel();
            string username = string.Empty;
            var authHeader = request.Request.Headers.TryGetValue("Authorization", out StringValues authorizationToken);
            var authToken = authorizationToken.ToString().Replace("Bearer", "").Trim();
            request.Request.Headers.TryGetValue("Timezone", out StringValues timezone);
            request.Request.Headers.TryGetValue("IPAddress", out StringValues ipAddress);
            request.Request.Headers.TryGetValue("LocationID", out StringValues locationID);
            TokenModel tokenModel = new TokenModel();
            try
            {
                if (!string.IsNullOrEmpty(authToken) || !string.IsNullOrWhiteSpace(authToken))
                {
                    var encryptData = GetDataFromToken(authToken);
                    if (encryptData != null && encryptData.Claims != null)
                    {

                        tokenModel.UserName = encryptData.Claims[0].Value;
                        tokenModel.UserTypeID = encryptData.Claims[1].Value;
                        tokenModel.UserID = encryptData.Claims[2].Value;
                        tokenModel.RoleID = encryptData.Claims[3].Value;
                        tokenModel.UserType = encryptData.Claims[4].Value;
                        tokenModel.Role = encryptData.Claims[5].Value;
                        
                    }
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            return tokenModel;
        }

        public static dynamic GetDataFromToken(string token)
        {
            try
            {
                var handler = new JwtSecurityTokenHandler();
                return handler.ReadToken(token);
            }
            catch
            {
                return null;
            }

        }

        public static string GenerateAlfaNumericNumber()
        {
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            char[] stringChars = new char[12];
            Random random = new Random();
            string RandomNumber;

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }
            RandomNumber = new String(stringChars);
            return RandomNumber;
        }
    }
}
