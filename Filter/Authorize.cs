﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using SynchrosignNew.Common;
using SynchrosignNew.Models;
using System; 
using System.ComponentModel.DataAnnotations;

namespace SynchrosignNew.Filters
{  
    public class AuthorizedAttribute : ActionFilterAttribute
    {
        
        public override void OnActionExecuting(ActionExecutingContext context)
        {
          
            TokenModel token = CommonMethods.GetTokenDataModel(context.HttpContext);
            if (string.IsNullOrEmpty(token.UserID) || string.IsNullOrEmpty(token.UserTypeID))
            //if (token.UserID <= 0 || token.UserTypeID != (Int32)Enums.UserTypeID.Admin)
            {
                context.Result = new UnauthorizedResult();
            }
        }
    }   
}
