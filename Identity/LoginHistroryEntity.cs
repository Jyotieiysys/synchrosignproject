﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SynchrosignNew.Identity
{
    public class LoginHistoryEntity
    {
        [Key]
        public Guid LoginHistoryID { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string AccountNo { get; set; }
     
        public DateTime LogInTime { get; set; }
    }
}
