﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SynchrosignNew.Identity
{
    public class UserEntity : IdentityUser<Guid>
    {
       // public Guid UserId { get; set; }
        
        public string FullName { get; set; }
        public string Email { get; set; }
        public string NormalizedEmail { get; set; }

        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string SelectNationality { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTimeOffset ModifiedDate { get; internal set; }
        public string AccountNo { get; set; }
        public string Status { get; set; }
        public string IsDeleted { get; set; }

        public string IPAddress { get; set; }

        public bool LoginShied { get; set; }
    }
}
