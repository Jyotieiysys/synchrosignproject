﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SynchrosignNew.Identity
{
    public class UserSecurityQuestion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int QuestionId { get; set; }
        public string UserId { get; set; }
        public string AccountId { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        
    }
}
