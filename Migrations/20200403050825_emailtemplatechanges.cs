﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SynchrosignNew.Migrations
{
    public partial class emailtemplatechanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                table: "AspNetUsers");

            migrationBuilder.CreateTable(
                name: "EmailTemplate",
                columns: table => new
                {
                    TemplateCode = table.Column<string>(nullable: false),
                    TemplateType = table.Column<string>(nullable: true),
                    TemplateName = table.Column<string>(nullable: true),
                    SMTPUsername = table.Column<string>(nullable: true),
                    SenderEmail = table.Column<string>(nullable: true),
                    SenderName = table.Column<string>(nullable: true),
                    SMTPPassword = table.Column<string>(nullable: true),
                    Subject = table.Column<string>(nullable: true),
                    Body = table.Column<string>(nullable: true),
                    HostName = table.Column<string>(nullable: true),
                    HostPort = table.Column<int>(nullable: false),
                    EnableSSL = table.Column<bool>(nullable: false),
                    TimeOut = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailTemplate", x => x.TemplateCode);
                });

            migrationBuilder.CreateTable(
                name: "GenericSettings",
                columns: table => new
                {
                    SettingID = table.Column<Guid>(nullable: false),
                    AdminAccountId = table.Column<string>(nullable: false),
                    SettingName = table.Column<string>(nullable: false),
                    SubSettingName = table.Column<string>(nullable: false),
                    DefaultTextValue20_1 = table.Column<string>(maxLength: 20, nullable: true),
                    DefaultTextValue20_2 = table.Column<string>(maxLength: 20, nullable: true),
                    DefaultTextValue50_1 = table.Column<string>(maxLength: 50, nullable: true),
                    DefaultTextValue50_2 = table.Column<string>(maxLength: 50, nullable: true),
                    DefaultTextValue100_1 = table.Column<string>(maxLength: 100, nullable: true),
                    DefaultTextValue100_2 = table.Column<string>(maxLength: 100, nullable: true),
                    DefaultTextValue250_1 = table.Column<string>(maxLength: 250, nullable: true),
                    DefaultTextValue250_2 = table.Column<string>(maxLength: 250, nullable: true),
                    DefaultTextMax = table.Column<string>(nullable: true),
                    DefaultTextMax1 = table.Column<string>(nullable: true),
                    DefaultTextMax2 = table.Column<string>(nullable: true),
                    DefaultTextMax3 = table.Column<string>(nullable: true),
                    DefaultTextMax4 = table.Column<string>(nullable: true),
                    DefalutInteger1 = table.Column<int>(nullable: false),
                    DefalutInteger2 = table.Column<int>(nullable: false),
                    DefalutInteger3 = table.Column<int>(nullable: false),
                    DefalutInteger4 = table.Column<int>(nullable: false),
                    DefalutInteger5 = table.Column<int>(nullable: false),
                    DefaultDecimal1 = table.Column<decimal>(nullable: false),
                    DefaultDecimal2 = table.Column<decimal>(nullable: false),
                    DefaultDecimal3 = table.Column<decimal>(nullable: false),
                    DefaultDecimal4 = table.Column<decimal>(nullable: false),
                    DefaultDecimal5 = table.Column<decimal>(nullable: false),
                    DefaultDateTime1 = table.Column<DateTime>(nullable: true),
                    DefaultDateTime2 = table.Column<DateTime>(nullable: true),
                    DefaultDateTime3 = table.Column<DateTime>(nullable: true),
                    DefaultDateTime4 = table.Column<DateTime>(nullable: true),
                    DefaultDateTime5 = table.Column<DateTime>(nullable: true),
                    DefaultBool1 = table.Column<bool>(nullable: false),
                    DefaultBool2 = table.Column<bool>(nullable: false),
                    DefaultBool3 = table.Column<bool>(nullable: false),
                    DefaultBool4 = table.Column<bool>(nullable: false),
                    DefaultBool5 = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GenericSettings", x => new { x.SettingID, x.AdminAccountId, x.SettingName, x.SubSettingName });
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmailTemplate");

            migrationBuilder.DropTable(
                name: "GenericSettings");

            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "AspNetUsers",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }
    }
}
