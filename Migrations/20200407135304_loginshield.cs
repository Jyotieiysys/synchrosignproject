﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SynchrosignNew.Migrations
{
    public partial class loginshield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LoginShields",
                columns: table => new
                {
                    ShieldId = table.Column<Guid>(nullable: false),
                    AccountNo = table.Column<string>(nullable: true),
                    MacAddress = table.Column<string>(nullable: true),
                    IpAddress = table.Column<string>(nullable: true),
                    Browser = table.Column<string>(nullable: true),
                    Active = table.Column<bool>(nullable: false),
                    Location = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoginShields", x => x.ShieldId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LoginShields");
        }
    }
}
