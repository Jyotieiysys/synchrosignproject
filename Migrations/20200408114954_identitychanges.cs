﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SynchrosignNew.Migrations
{
    public partial class identitychanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "LoginShied",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LoginShied",
                table: "AspNetUsers");
        }
    }
}
