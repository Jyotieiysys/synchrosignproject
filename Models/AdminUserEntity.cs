﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SynchrobitExchange.Models
{
    public class AdminUserEntity : IdentityUser<Guid>
    {
        public string FullName { get; set; }
        public string AccountNo { get; set; }

        public string ContactNo { get; set; }

        public bool Active { get; set; }
        public string Address { get; set; }

        public string City { get; set; }

        public string Country { get; set; }
        
        public DateTimeOffset? CreatedAt { get; set; }
        
        public DateTimeOffset? LastModifiedAt { get; set; }
        public bool LoginAlert { get; set; }
        public bool CodeCard { get; set; }
        public bool TwoFactorTrans { get; set; }
        public bool IsBlocked { get; set; }
        public long TicketUserId { get; set; }
        public bool ProfileCompleted { get; set; }
        public bool LoginShield { get; set; }

        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }

        public bool IsWithdrawal2FA { get; set; }
        public bool IsChangePassword2FA { get; set; }
        public bool IsApiKey2FA { get; set; }
    }
}
