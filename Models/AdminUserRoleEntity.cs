﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SynchrobitExchange.Models;

namespace SynchrobitExchange.Models
{
    public class AdminUserRoleEntity : IdentityRole<Guid>
    {
        public AdminUserRoleEntity(Task<AdminUserRoleEntity> role)
            : base()
        { }

        public AdminUserRoleEntity(string role)
            : base(role)
        { }
        public AdminUserRoleEntity() { }


    }
}
