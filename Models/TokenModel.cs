﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SynchrosignNew.Models
{
    public class TokenModel
    {
        public string UserName { get; set; }
        public string UserID { get; set; }
        public string UserTypeID { get; set; }
        public string  RoleID { get; set; }
        public string  UserType { get; set; }
        public string Role { get; set; }
       
    }
}
