﻿using System;
using System.Collections.Generic;
using System.Text;
using Hangfire;
using Jose;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SynchrobitExchange.Models;
using SynchrosignNew.Areas.User.Context;
using SynchrosignNew.Areas.User.InterFace;
using SynchrosignNew.Areas.User.Services;
using SynchrosignNew.CustomTokenProvider;
using Microsoft.AspNetCore.HttpOverrides;
using SynchrosignNew.Identity;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.OpenApi.Models;
using SynchrosignNew.Admin.IServices;
using SynchrosignNew.Admin.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using SynchrosignNew.Service;

namespace SynchrosignNew
{
    public class Startup
    {
        private readonly string DBConnection;
        private readonly TokenProviderOptions _options;
        private const string _secretKey = "SynchrosignNewProject";
        private readonly SymmetricSecurityKey _signingKey;
        public Startup(IConfiguration configuration,
              IOptions<TokenProviderOptions> options)
        {
            _options = options.Value;
            Configuration = configuration;
            _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_secretKey));
            DBConnection = (Convert.ToBoolean(Configuration.GetValue<string>("Environment:Development")) ? Configuration.GetValue<string>("ConnectionStrings:DevServer") : Configuration.GetValue<string>("ConnectionStrings:LiveServer"));
        }

        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {          
            services.Configure<FormOptions>(Option =>
            {
                Option.MultipartBodyLengthLimit = 200000000;

            });

            services.AddControllers();
            services.AddDbContext<SynchrosignContext>(options =>
            options.UseSqlServer(DBConnection));

            // services.AddIdentity<AdminUserEntity, AdminUserRoleEntity>().AddEntityFrameworkStores<SynchrosignContextAdmin>().AddDefaultTokenProviders();
            services.AddIdentity<UserEntity, UserRoleEntity>().AddEntityFrameworkStores<SynchrosignContext>()
          .AddTokenProvider("UserDefault", typeof(DataProtectorTokenProvider<>).MakeGenericType(typeof(UserEntity)))
        .AddTokenProvider("UserDefaultEmail", typeof(EmailTokenProvider<>).MakeGenericType(typeof(UserEntity)))
        .AddTokenProvider("UserDefaultPhone", typeof(PhoneNumberTokenProvider<>).MakeGenericType(typeof(UserEntity)));

            services.AddCors(options => options.AddPolicy("AllowCors",
                            builder =>
                            {
                                builder.AllowAnyOrigin()
                                    .WithMethods("GET", "PUT", "POST", "DELETE")
                                    .AllowAnyHeader();
                            })
                        );

            services.AddHangfire(x => x.UseSqlServerStorage(DBConnection));


            services.AddTransient<IUserService, UserService>();
            services.AddTransient<ICommonService, CommonService>();
            services.AddTransient<IAdminService, AdminService>();
            services.AddMvc();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "SynchrosignNew API",
                    Version = "v1"
                });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Please insert JWT with Bearer into field",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement {
                   {
                     new OpenApiSecurityScheme
                     {
                       Reference = new OpenApiReference
                       {
                         Type = ReferenceType.SecurityScheme,
                         Id = "Bearer"
                       }
                      },
                      new string[] { }
                    }
                  });
            });

            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));
            services.Configure<JwtIssuerOptions>(options =>
            {
                options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
                options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
            });

        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)],

                ValidateAudience = true,
                ValidAudience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)],

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _signingKey,

                RequireExpirationTime = true,
                ValidateLifetime = true,

                ClockSkew = TimeSpan.Zero
            };

            var options = new BackgroundJobServerOptions { WorkerCount = 1 };
            app.UseHangfireDashboard("/jobdashboard");
            app.UseHangfireServer(options); 
            app.UseCors("XCoins");
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Synchrosign Api");

            });

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
